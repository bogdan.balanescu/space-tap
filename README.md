# Space Tap

Date of development: Sep 2016

A space shooter game which challenges the player to travel in space for as long as possible, avoiding and shooting asteroids in order to keep the spaceship alive.
Language: React Native
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { MainScene } from './sources/scenes/MainScene.js'

class TapTapTap extends Component {
    constructor() {
        super();
    }

    render() {
        return (
            <MainScene>
            </MainScene>
        );
    }
}

AppRegistry.registerComponent('TapTapTap', () => TapTapTap);

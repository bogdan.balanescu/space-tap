/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Dimensions, Image, View } from 'react-native';
import { IMAGE_ASSETS, SPEEDS } from './../miscellaneous/variables.js'

export class Ammo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ammo: []
        };

        this.updateAmmo = this.updateAmmo.bind(this);

        this.addAmmo = this.addAmmo.bind(this);
        this.maxY = Dimensions.get('window').height;
        this.maxX = Dimensions.get('window').width;
        this.destroyAmmoAtIndex = this.destroyAmmoAtIndex.bind(this);
        this.getAmmoPositions = this.getAmmoPositions.bind(this);
    }

    updateAmmo() {
        var ammo = this.state.ammo;
        for (var i = 0; i < ammo.length; i++) {
            var currentAmmo = ammo[i];
            currentAmmo.position.y -= currentAmmo.velocity.y;
            currentAmmo.rotation += currentAmmo.rotationVelocity;
            currentAmmo.rotation %= 360;
            if (currentAmmo.position.y <= -32) { // TODO - refactor - don't hardcode
                ammo.splice(i, 1);
            }
        }
        this.setState({ammo: ammo});
    }

    addAmmo(asteroidLastPosition) {
        var ammo = this.state.ammo;
        var rotation = Math.floor(Math.random() * 360);
        var rotationVelocity = Math.random() * SPEEDS.AMMO.rotationVelocity.randomFactor - SPEEDS.AMMO.rotationVelocity.staticFactor;
        ammo.push({
            position: {x: asteroidLastPosition.x, y: asteroidLastPosition.y},
            velocity: asteroidLastPosition.velocity,
            rotation: rotation,
            rotationVelocity: rotationVelocity
        });
        this.setState({ammo: ammo});
    }

    destroyAmmoAtIndex(index) {
        var ammo = this.state.ammo;
        ammo.splice(index, 1);
        this.setState({ammo: ammo});
    }

    getAmmoPositions() {
        var ammoPositions = [];
        this.state.ammo.map(function(item, i) {
            ammoPositions.push({x: item.position.x, y: item.position.y,
                width: 32, height: 32}); // TODO - refactor - don't hardcode
        });
        return ammoPositions;
    }

    render() {
        return (
            <View style={{position: 'absolute', bottom: 0, left: 0}}>
                {this.state.ammo.map(function(item, i) {
                    return item.position.y > -32 ?
                        (
                            <Image key={i} source={IMAGE_ASSETS.AMMO}
                                   style={{position: 'absolute', bottom: item.position.y, left: item.position.x,
                                   transform: [{rotate: item.rotation + 'deg'}]}}>
                            </Image>
                        ) :
                        null
                }, this)}
            </View>
        );
    }
}


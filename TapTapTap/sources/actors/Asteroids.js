/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Dimensions, Image, View } from 'react-native';
import { TIMERS, ASTEROIDS, LEVELS } from './../miscellaneous/variables.js'

export class Asteroids extends Component {
    constructor(props) {
        super(props);
        this.state = {
            asteroids: []
        };

        this.updateAsteroids = this.updateAsteroids.bind(this);

        this.addAsteroids = this.addAsteroids.bind(this);
        this.maxY = Dimensions.get('window').height;
        this.maxX = Dimensions.get('window').width;
        this.addAsteroid = this.addAsteroid.bind(this);

        this.getAsteroidsPositions = this.getAsteroidsPositions.bind(this);

        this.level = undefined;
        this.levelInformation = undefined;
    }

    componentDidMount() {
        this.level = this.props.getLevel();
        this.levelInformation = LEVELS[this.level];
        this.addAsteroids(this.levelInformation.asteroidsCount);
    }

    updateAsteroids() {
        var asteroids = this.state.asteroids;
        for (var i = 0; i < asteroids.length; i++) {
            var asteroid = asteroids[i];
            asteroid.position.y -= asteroid.velocity.y;
            asteroid.rotation += asteroid.rotationVelocity;
            asteroid.rotation %= 360;
            if (asteroid.position.y <= -asteroid.asteroidProperties.size) {
                asteroids.splice(i, 1);
                this.addAsteroids(1);
            }
        }
        this.setState({asteroids: asteroids});
        var level = this.props.getLevel();
        if (level > this.level) {
            var oldAsteroidsCount = this.levelInformation.asteroidsCount;
            this.level = this.props.getLevel();
            this.levelInformation = LEVELS[this.level];
            var newAsteroidsCount = this.levelInformation.asteroidsCount;
            this.addAsteroids(newAsteroidsCount - oldAsteroidsCount); // should be positive :D
        }
    }

    addAsteroid() {
        var asteroids = this.state.asteroids;
        var index = Math.floor(Math.random() * ASTEROIDS.length);
        var asteroidProperties = ASTEROIDS[index];
        var xPosition = Math.floor(Math.random() * this.maxX);
        if (xPosition > this.maxX - asteroidProperties.size) {
            xPosition = this.maxX - asteroidProperties.size;
        }
        var yVelocity = Math.random() * this.levelInformation.randomVelocityFactor + this.levelInformation.staticVelocityFactor;
        var rotation = Math.floor(Math.random() * 360);
        var rotationVelocity = Math.random() * this.levelInformation.randomRotationFactor - this.levelInformation.staticRotationFactor;
        asteroids.push({
            asteroidProperties: asteroidProperties,
            position: {x: xPosition, y: this.maxY + asteroidProperties.size},
            velocity: {x: 0, y: yVelocity},
            rotation: rotation,
            rotationVelocity: rotationVelocity
        });
        this.setState({asteroids: asteroids});
    }

    addAsteroids(count) {
        for (var i = 0; i < count; i++) {
            var schedulePush = Math.floor(TIMERS.ASTEROID_MINIMUM_SPAWN_TIME * i);
            setTimeout(this.addAsteroid, schedulePush);
        }
    }

    getAsteroidsPositions() {
        var asteroidsPositions = [];
        this.state.asteroids.map(function(item, i) {
            asteroidsPositions.push({x: item.position.x, y: item.position.y,
                                     width: item.asteroidProperties.size, height: item.asteroidProperties.size,
                                     velocity: item.velocity});
        });
        return asteroidsPositions;
    }

    destroyAsteroidAtIndex(index) {
        var asteroids = this.state.asteroids;
        asteroids.splice(index, 1);
        this.setState({asteroids: asteroids});
        this.addAsteroid(1);
    }

    render() {
        return (
            <View style={{position: 'absolute', bottom: 0, left: 0}}>
                {this.state.asteroids.map(function(item, i) {
                    return item.position.y > -item.asteroidProperties.size ?
                        (
                            <Image key={i} source={item.asteroidProperties.image}
                                   style={{position: 'absolute', bottom: item.position.y, left: item.position.x,
                                   transform: [{rotate: item.rotation + 'deg'}]}}>
                            </Image>
                        ) :
                        null
                }, this)}
            </View>
        );
    }
}


/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Dimensions, Image, View } from 'react-native';
import { TIMERS, IMAGE_ASSETS, SPEEDS } from './../miscellaneous/variables.js'

export class Bullets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bullets: [],
            maxBullets: props.maxBullets,
            bulletsCount: 0
        };

        this.updateBullets = this.updateBullets.bind(this);

        this.canFire = true;
        this.addBullet = this.addBullet.bind(this);
        this.cooldownWeapon = this.cooldownWeapon.bind(this);

        this.maxY = Dimensions.get('window').height;

        this.getBulletsPositions = this.getBulletsPositions.bind(this);
        this.destroyBulletAtIndex = this.destroyBulletAtIndex.bind(this);

        this.addToMaxBullets = this.addToMaxBullets.bind(this);
    }

    updateBullets() {
        var bullets = this.state.bullets;
        if (bullets.length == 0) {
            return; // for performance optimization
        }
        for (var i = 0; i < bullets.length; i++) {
            var bullet = bullets[i];
            bullet.position.y += bullet.velocity.y;
            if (bullet.position.y >= this.maxY) {
                bullets.splice(i, 1);
            }
        }
        this.setState({bullets: bullets});
    }

    cooldownWeapon() {
        this.canFire = true; // TODO - message that weapon is no longer on cooldown
    }

    addBullet(count, lastPosition) {
        var bullets = this.state.bullets;
        if (this.state.bulletsCount >= this.state.maxBullets) {
            return false; // TODO message that cannot fire anymore - maxBullets has been reached
        }
        if (this.canFire == false) {
            return false; // TODO message that cannot fire - has to cooldown
        }
        this.canFire = false;
        setTimeout(this.cooldownWeapon, TIMERS.COOLDOWN_TIME); // TODO - message that weapons is on cooldown
        for (var i = 0; i < count; i++) {
            var y = Math.random() * SPEEDS.BULLET.randomFactor + SPEEDS.BULLET.staticFactor;
            bullets.push({
                position: {x: lastPosition.x, y: lastPosition.y},
                velocity: {x: 0, y: y}
            })
        }
        this.setState({bullets: bullets, bulletsCount: this.state.bulletsCount + count});
        return true;
    }

    getBulletsPositions() {
        var bulletsPositions = [];
        this.state.bullets.map(function(item, i) {
            bulletsPositions.push({x: item.position.x, y: item.position.y,
                width: 8, height: 16});
        });
        return bulletsPositions;
    }

    destroyBulletAtIndex(index) {
        var bullets = this.state.bullets;
        bullets.splice(index, 1);
        this.setState({bullets: bullets});
    }

    addToMaxBullets(value) {
        this.setState({maxBullets: this.state.maxBullets + value});
    }

    render() {
        return (
            <View style={{position: 'absolute', bottom: 0, left: 0}}>
                {this.state.bullets.map(function(item, i) {
                    return item.position.y > 0 && item.position.y <= this.maxY ?
                        (
                            <Image key={i} source={IMAGE_ASSETS.BULLET}
                                   style={{position: 'absolute', bottom: item.position.y, left: item.position.x}}>
                            </Image>
                        ) :
                        null
                }, this)}
            </View>
        );
    }
}


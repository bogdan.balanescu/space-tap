/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Dimensions, Image } from 'react-native';
import { SPACESHIP_TYPE } from './../miscellaneous/variables.js'

export class Spaceship extends Component {
    constructor(props) {
        super(props);
        this.state = {
            x: Dimensions.get('window').width / 2 - 35,
            y: 0
        };
        this.move = this.move.bind(this);
        this.maxX = Dimensions.get('window').width;
    }

    move(xValue) {
        var spaceship = this.props.spaceshipType;
        if (xValue < 0 && this.state.x + xValue <= 0) {
            this.setState({x: 0});
            return;
        }
        else if (xValue > 0 && this.state.x >= this.maxX - spaceship.style.width) {
            this.setState({x: this.maxX - spaceship.style.width});
            return;
        }
        this.setState({x: this.state.x + xValue});
    }

    getPosition() {
        return {
            x: this.state.x,
            y: this.state.y
        };
    }

    getCollisionPosition() {
        return {
            x: this.state.x + this.props.spaceshipType.wingWidth,
            y: this.state.y + 20, // reversed coordinates system - to avoid the tail and not the head // TODO - refactor hardcoded value
            width: this.props.spaceshipType.style.width - 2 * this.props.spaceshipType.wingWidth, // don't mind the wings
            height: this.props.spaceshipType.style.height - 20 // don't mind the tail // TODO - refactor hardcoded value
        };
    }

    render() {
        var image = this.props.spaceshipType.image;
        var style = this.props.spaceshipType.style;
        return (
            <Image source={image} style={{position: 'absolute', bottom: this.state.y, left: this.state.x,
                                    width: style.width, height: style.height}}>
            </Image>
        );
    }
}


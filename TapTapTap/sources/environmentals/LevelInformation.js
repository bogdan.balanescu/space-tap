/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { styles } from './../miscellaneous/styles.js'
import { TIMERS, LEVELS } from './../miscellaneous/variables.js'

export class LevelInformation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            score: 0,
            maxBullets: props.initialBulletsCount,
            bulletsCount: 0
        };
        this.renderInterval = undefined;
        this.updateLevelInformation = this.updateLevelInformation.bind(this);

        this.stopRendering = this.stopRendering.bind(this);

        this.addScore = this.addScore.bind(this);

        this.addToMaxBullets = this.addToMaxBullets.bind(this);
    }

    componentDidMount() {
    this.renderInterval = setInterval(this.updateLevelInformation, TIMERS.SCORE_FRAME_TIME);
}

    componentWillUnmount() {
        clearInterval(this.renderInterval);
    }

    shouldComponentUpdate() {
        return true;
    }

    stopRendering() {
        clearInterval(this.renderInterval);
    }

    updateLevelInformation() {
        this.setState({score: this.state.score + 10}); // TODO - refactor - don't hardcode
    }

    getLevel() {
        var level = Math.floor(this.state.score / 200); // TODO - refactor - don't hardcode
        if (level >= LEVELS.length) {
            level = LEVELS.length - 1;
        }
        return level;
    }

    getScore() {
        return this.state.score;
    }

    addBulletCount(value) {
        this.setState({bulletsCount: this.state.bulletsCount + value});
    }

    addScore(value) {
        this.setState({score: this.state.score + value});
    }

    addToMaxBullets(value) {
        this.setState({maxBullets: this.state.maxBullets + value});
    }

    render() {
        var bullets = this.state.maxBullets - this.state.bulletsCount;
        return (
            <View style={styles.information}>
                <Text style={styles.text}>
                    Bullets left: {bullets} | Score: {this.state.score}
                </Text>
            </View>
        );
    }
}


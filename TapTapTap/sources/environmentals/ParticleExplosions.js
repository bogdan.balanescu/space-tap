/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View } from 'react-native';
import { styles } from './../miscellaneous/styles.js'
import { TIMERS } from './../miscellaneous/variables.js'

export class ParticleExplosions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            particles: []
        };
        this.addParticles = this.addParticles.bind(this);
        this.updateParticles = this.updateParticles.bind(this);
        this.renderInterval = undefined;

        this.stopRendering = this.stopRendering.bind(this);
    }

    componentDidMount() {
        this.renderInterval = setInterval(this.updateParticles, TIMERS.EXPLOSION_FRAME_TIME);
    }

    componentWillUnmount() {
        clearInterval(this.renderInterval);
    }

    shouldComponentUpdate() {
        return true;
    }

    stopRendering() {
        clearInterval(this.renderInterval);
    }

    updateParticles() {
        var particles = this.state.particles;
        for (var i = 0; i < particles.length; i++) {
            var particle = particles[i];
            particle.position.x += particle.velocity.x;
            particle.position.y += particle.velocity.y;
            particle.size -= 1;
            if (particle.size <= 0) {
                particles.splice(i, 1);
            }
        }
        this.setState({particles: particles});
    }

    addParticles(explosionType, lastPosition) {
        var particles = this.state.particles;
        for (var i = 0; i < explosionType.count; i++) {
            var x = Math.random() * explosionType.velocityRandomFactor + explosionType.velocityStaticFactor;
            var y = Math.random() * explosionType.velocityRandomFactor + explosionType.velocityStaticFactor;
            lastPosition.x += x * explosionType.disperseFactor;
            lastPosition.y += y * explosionType.disperseFactor;
            particles.push({
                position: {x: lastPosition.x, y: lastPosition.y},
                velocity: {x: x, y: y},
                size: explosionType.size
            });
        }
        this.setState({particles: particles});
    }

    render() {
        return this.props.coordinatesOrientationBottom == true ? // TODO - find a better solution than code repetition
        (
            <View style={{position: 'absolute', bottom: 0, left: 0}}>
                {this.state.particles.map(function(item, i) {
                    return item.size > 0 ?
                        (
                            <View key={i} style={[styles.explosion, {position: 'absolute',
                                    bottom: item.position.y, left: item.position.x,
                                    width: item.size, height: item.size}]}>
                            </View>
                        ) :
                        null
                }, this)}
            </View>
        ):
        (
            <View style={{position: 'absolute', top: 0, left: 0}}>
                {this.state.particles.map(function(item, i) {
                    return item.size > 0 ?
                        (
                            <View key={i} style={[styles.explosion, {position: 'absolute',
                                    top: item.position.y, left: item.position.x,
                                    width: item.size, height: item.size}]}>
                            </View>
                        ) :
                        null
                }, this)}
            </View>
        );
    }
}


/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, Text, Modal, TouchableWithoutFeedback } from 'react-native';
import { styles } from './../miscellaneous/styles.js'
import { SpaceBackground } from './SpaceBackground.js'

import { sounds } from './../miscellaneous/sounds.js'

export class QuestionModal extends Component {
    constructor(props) {
        super(props);

        this.pressedButton = this.pressedButton.bind(this);
    }

    pressedButton(func) {
        sounds.buttonPress.replay();
        func();
    }

    render() {
        return (
            <Modal
                animationType={"slide"}
                transparent={this.props.transparent}
                visible={this.props.visible}
                onRequestClose={() => {}}>
                <View style={this.props.transparent == true ? styles.transparentContainer: styles.container}>
                    {
                        this.props.transparent == false ?
                        <SpaceBackground>
                        </SpaceBackground> :
                        null
                    }
                    <Text style={styles.text}>
                        {this.props.question}
                    </Text>
                    <View>
                        <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.close)}>
                            <View style={[styles.button, styles.container]}>
                                <Text style={styles.text}>
                                    Yes
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.cancel)}>
                            <View style={[styles.button, styles.container]}>
                                <Text style={styles.text}>
                                    No
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </View>
            </Modal>
        );
    }
}


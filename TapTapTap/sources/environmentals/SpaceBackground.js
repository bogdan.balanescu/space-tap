/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import { styles } from './../miscellaneous/styles.js'
import { TIMERS, EXPLOSIONS } from './../miscellaneous/variables.js'
import { ParticleExplosions } from './ParticleExplosions.js'

export class SpaceBackground extends Component {
    constructor(props) {
        super(props);
        this.updateSpaceBackground = this.updateSpaceBackground.bind(this);
        this.maxX = Dimensions.get('window').width;
        this.maxY = Dimensions.get('window').height;
        this.renderInterval = undefined;

        this.stopRendering = this.stopRendering.bind(this);
    }

    componentDidMount() {
        this.renderInterval = setTimeout(this.updateSpaceBackground, Math.random() * TIMERS.SPACE_BACKGROUND_EXPLOSIONS);
    }

    componentWillUnmount() {
        clearInterval(this.renderInterval);
    }

    shouldComponentUpdate() {
        return true;
    }

    stopRendering() {
        clearTimeout(this.renderInterval);
        this.refs.explosions.stopRendering(); // not necessarily needed
    }

    updateSpaceBackground() {
        var explosionsCount = Math.floor(Math.random() * 3) + 1; // TODO - refactor hardcoded value
        for (var i = 0; i < explosionsCount; i++) {
            var x = Math.random() * this.maxX;
            var y = Math.random() * this.maxY;
            this.refs.explosions.addParticles(EXPLOSIONS.SPACE_BACKGROUND_EXPLOSIONS, {x: x, y: y});
        }
        this.renderInterval = setTimeout(this.updateSpaceBackground, Math.random() * TIMERS.SPACE_BACKGROUND_EXPLOSIONS);
    }

    render() {
        return (
            <ParticleExplosions ref="explosions">
            </ParticleExplosions>
        );
    }
}


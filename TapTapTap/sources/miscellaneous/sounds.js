// Import the react-native-sound module
var Sound = require('react-native-sound');

class GameSound {
    constructor(fileName, volume) {
        this.volume = volume || 1;
        this.sound = new Sound(fileName, Sound.MAIN_BUNDLE, (error) => {
                if (error) {
                    console.log('failed to load the sound', error);
                } else { // loaded successfully
                    // TODO - what should be done to sound
                    this.sound.setVolume(0);
                }
            });
        this.play = this.play.bind(this);
        this.stop = this.stop.bind(this);
        this.replay = this.replay.bind(this);
        this.setVolume = this.setVolume.bind(this);
    }

    replay() {
        //this.sound.stop();
        this.sound.setCurrentTime(0);
        this.sound.play(this.onEnd);
    }

    play(count) {
        count = count || 0;
        this.sound.setNumberOfLoops(count);
        this.sound.play(this.onEnd);
    }

    stop() {
        this.sound.pause();
        this.sound.stop();
    }

    onEnd(success) {
        if (success) {
            // TODO
            //console.log('successfully finished playing');
        } else {
            console.log('playback failed due to audio decoding errors');
        }
    }

    setVolume(percentage) {
        percentage = percentage < 1 ? percentage: 1;
        this.sound.setVolume(this.volume * percentage);
    }

}

export var sounds = {
    mainMenu: new GameSound('music_game_menu_v001.mp3', 0.2),
    playingSound: new GameSound('music_eight_bit_mayhem.mp3', 0.1),
    buttonPress: new GameSound('fx_multimedia_button_click_013.mp3', 1),
    fireBullet: new GameSound('fx_explosion_small.mp3', 0.1),
    asteroidExplosion: new GameSound('fx_explosion_slight_distant_explosion.mp3', 1),
    ammoCollected: new GameSound('fx_shotgun_reload_slight.mp3', 0.2),
    setVolume: function(volume) {
        this.mainMenu.setVolume(volume);
        this.playingSound.setVolume(volume);
        this.buttonPress.setVolume(volume);
        this.fireBullet.setVolume(volume);
        this.asteroidExplosion.setVolume(volume);
        this.ammoCollected.setVolume(volume);
    }
};


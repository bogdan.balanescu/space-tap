import { StyleSheet, Dimensions } from 'react-native';

var maxWidth = Dimensions.get('window').width;
var maxHeight = Dimensions.get('window').height;
var mainColor = 'orange';
var backColor = 'black';

function createStyles() {
    return StyleSheet.create({
        container: {
            flex: 1,
            alignSelf: 'stretch',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: backColor
        },
        transparentContainer: {
            flex: 1,
            alignSelf: 'stretch',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'transparent'
        },
        scrollableTransparentContainer: {
            paddingTop: 50,
            paddingBottom: 50,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'transparent'
        },
        explosion: {
            backgroundColor: mainColor
        },
        title: {
            color: mainColor,
            fontSize: 25,
            textAlign: 'center',
            fontWeight: 'bold'
        },
        subtitle: {
            color: mainColor,
            fontSize: 20,
            textAlign: 'center'
        },
        text: {
            color: mainColor,
            fontSize: 15,
            textAlign: 'center'
        },
        textInput: {
            color: mainColor,
            fontSize: 15,
            width: maxWidth / 2,
            textAlign: 'center'
        },
        slider: {
            width: maxWidth / 2,
        },
        button: {
            borderWidth: 2,
            borderColor: mainColor,
            borderRadius: 5,
            width: 100,
            margin: 5,
            padding: 5
        },
        information: {
            position: 'absolute',
            top: 0,
            right: 0,
            opacity: 0.5
        }
    });
}

export var styles;

function reloadStyles() {
    styles = createStyles();
}

function setNightMode() {
    mainColor = 'orange';
    backColor = 'black';
    reloadStyles();
}

function setDayMode() {
    mainColor = 'black';
    backColor = 'yellow';
    reloadStyles();
}

export function setColorMode(value) {
    if (value == 'day') {
        setDayMode();
    }
    else if (value == 'night') {
        setNightMode();
    }
}

reloadStyles();

// miscellaneous variables and objects

function speeds() {
    return {
        BULLET: {
            randomFactor: 1 * (TIMERS.FRAME_TIME / 16),
            staticFactor: 6 * (TIMERS.FRAME_TIME / 16)
        },
        AMMO: {
            rotationVelocity: {
                randomFactor: 7 * (TIMERS.FRAME_TIME / 16),
                staticFactor: 3.5 * (TIMERS.FRAME_TIME / 16)
            }
        }
    }
}

function levels() {
    return [
        { // 0
            asteroidsCount: 5,
            randomVelocityFactor: 1 * (TIMERS.FRAME_TIME / 16),
            staticVelocityFactor: 3 * (TIMERS.FRAME_TIME / 16),
            randomRotationFactor: 10 * (TIMERS.FRAME_TIME / 16),
            staticRotationFactor: 5 * (TIMERS.FRAME_TIME / 16)
        },
        { // 1
            asteroidsCount: 7,
            randomVelocityFactor: 1 * (TIMERS.FRAME_TIME / 16),
            staticVelocityFactor: 3 * (TIMERS.FRAME_TIME / 16),
            randomRotationFactor: 12 * (TIMERS.FRAME_TIME / 16),
            staticRotationFactor: 6 * (TIMERS.FRAME_TIME / 16)
        },
        { // 2
            asteroidsCount: 8,
            randomVelocityFactor: 1 * (TIMERS.FRAME_TIME / 16),
            staticVelocityFactor: 3.5 * (TIMERS.FRAME_TIME / 16),
            randomRotationFactor: 12 * (TIMERS.FRAME_TIME / 16),
            staticRotationFactor: 6 * (TIMERS.FRAME_TIME / 16)
        },
        { // 3
            asteroidsCount: 8,
            randomVelocityFactor: 1 * (TIMERS.FRAME_TIME / 16),
            staticVelocityFactor: 4 * (TIMERS.FRAME_TIME / 16),
            randomRotationFactor: 14 * (TIMERS.FRAME_TIME / 16),
            staticRotationFactor: 7 * (TIMERS.FRAME_TIME / 16)
        },
        {// 4
            asteroidsCount: 9,
            randomVelocityFactor: 1 * (TIMERS.FRAME_TIME / 16),
            staticVelocityFactor: 4.5 * (TIMERS.FRAME_TIME / 16),
            randomRotationFactor: 14 * (TIMERS.FRAME_TIME / 16),
            staticRotationFactor: 7 * (TIMERS.FRAME_TIME / 16)
        },
        { // 5
            asteroidsCount: 10,
            randomVelocityFactor: 1.5 * (TIMERS.FRAME_TIME / 16),
            staticVelocityFactor: 4.5 * (TIMERS.FRAME_TIME / 16),
            randomRotationFactor: 14 * (TIMERS.FRAME_TIME / 16),
            staticRotationFactor: 7 * (TIMERS.FRAME_TIME / 16)
        },
        { // 6
            asteroidsCount: 11,
            randomVelocityFactor: 1.5 * (TIMERS.FRAME_TIME / 16),
            staticVelocityFactor: 5 * (TIMERS.FRAME_TIME / 16),
            randomRotationFactor: 14 * (TIMERS.FRAME_TIME / 16),
            staticRotationFactor: 7 * (TIMERS.FRAME_TIME / 16)
        },
        { // 7
            asteroidsCount: 12,
            randomVelocityFactor: 1.5 * (TIMERS.FRAME_TIME / 16),
            staticVelocityFactor: 5.5 * (TIMERS.FRAME_TIME / 16),
            randomRotationFactor: 14 * (TIMERS.FRAME_TIME / 16),
            staticRotationFactor: 7 * (TIMERS.FRAME_TIME / 16)
        },
        { // 8
            asteroidsCount: 13,
            randomVelocityFactor: 2 * (TIMERS.FRAME_TIME / 16),
            staticVelocityFactor: 6 * (TIMERS.FRAME_TIME / 16),
            randomRotationFactor: 14 * (TIMERS.FRAME_TIME / 16),
            staticRotationFactor: 7 * (TIMERS.FRAME_TIME / 16)
        }
    ]
}

function explosions() {
    return {
        TAPPING_EXPLOSION: {
            count: 15,
            size: 10,
            velocityRandomFactor: 10,
            velocityStaticFactor: -5,
            disperseFactor: 1
        },
        FIRING_EXPLOSION: {
            count: 10,
            size: 5,
            velocityRandomFactor: 10,
            velocityStaticFactor: -5,
            disperseFactor: 0
        },
        ASTEROID_EXPLOSION: {
            count: 20,
            size: 13,
            velocityRandomFactor: 10,
            velocityStaticFactor: -5,
            disperseFactor: 1
        },
        SPACESHIP_EXPLOSION: {
            count: 30,
            size: 20,
            velocityRandomFactor: 10,
            velocityStaticFactor: -5,
            disperseFactor: 1
        },
        SPACE_BACKGROUND_EXPLOSIONS: {
            count: 5,
            size: 5,
            velocityRandomFactor: 10,
            velocityStaticFactor: -5,
            disperseFactor: 0
        }
    }
}

function imageAssets() {
    return { // bullet, arrows, fire button, etc. <<< images
        BULLET: require('./../../images/bullet-trimmed.png'),
        LEFT_ARROW: require('./../../images/left-arrow.png'),
        RIGHT_ARROW: require('./../../images/right-arrow.png'),
        FIRE_BUTTON: require('./../../images/fire-button.png'),
        AMMO: require('./../../images/ammo.png')
    }
}

function asteroids() {
    return [
        {
            image: require('./../../images/asteroid-1-32.png'),
            size: 32
        },
        {
            image: require('./../../images/asteroid-1-64.png'),
            size: 64
        },
        //{
        //    image: require('./images/asteroid-1-128.png'),
        //    size: 128
        //},
        {
            image: require('./../../images/asteroid-2-32.png'),
            size: 32
        },
        {
            image: require('./../../images/asteroid-2-64.png'),
            size: 64
        },
        //{
        //    image: require('./images/asteroid-2-128.png'),
        //    size: 128
        //},
        {
            image: require('./../../images/asteroid-3-32.png'),
            size: 32
        },
        {
            image: require('./../../images/asteroid-3-64.png'),
            size: 64
        },
        //{
        //    image: require('./images/asteroid-3-128.png'),
        //    size: 128
        //}
    ]
}

function spaceshipType() {
    return {
        SMALL: {
            label: 'SMALL',
            image: require('./../../images/F5S1-orange-trimmed.png'),
            taps: 10,
            style: {
                width: 60,
                height: 100
            },
            wingWidth: 21,
            movingDistance: 5 * (TIMERS.FRAME_TIME / 16)
        },
        MEDIUM: {
            label: 'MEDIUM',
            image: require('./../../images/F5S2-orange-trimmed.png'),
            taps: 25,
            style: {
                width: 71,
                height: 110
            },
            wingWidth: 9,
            movingDistance: 5 * (TIMERS.FRAME_TIME / 16)
        },
        LARGE: {
            label: 'LARGE',
            image: require('./../../images/F5S3-orange-trimmed.png'),
            taps: 35,
            style: {
                width: 68,
                height: 120
            },
            wingWidth: 18,
            movingDistance: 5 * (TIMERS.FRAME_TIME / 16)
        },
        EPIC: {
            label: 'EPIC',
            image: require('./../../images/F5S4-orange-trimmed.png'),
            taps: Number.MAX_SAFE_INTEGER,
            style: {
                width: 68,
                height: 120
            },
            wingWidth: 23,
            movingDistance: 5 * (TIMERS.FRAME_TIME / 16)
        }
    }
}

export var TIMERS = {
    COUNTING_TAPS: 3000, // TODO - adjust time
    PICKING_MONSTER: 1000,
    PLAYING_GAME: 1000,
    SPACESHIP_FRAME: 10,
    FRAME_TIME: 66, // 60 FPS -> // TODO - change to 30FPS (or 45FPS) - it lags considerably - 45FPS now - 30FPS now
    EXPLOSION_FRAME_TIME: 33, // 30 FPS
    COOLDOWN_TIME: 500, // bullets cooldown time
    ASTEROID_MINIMUM_SPAWN_TIME: 1000,
    ASTEROID_MAXIMUM_SPAWN_TIME: 2000,
    SCORE_FRAME_TIME: 500,
    SPACE_BACKGROUND_EXPLOSIONS: 1000, // random
    ENDING_GAME: 33
};

export var SPEEDS;
export var LEVELS;
export var EXPLOSIONS;
export var IMAGE_ASSETS;
export var ASTEROIDS;
export var SPACESHIP_TYPE;

export function reloadVariables() {
    SPEEDS = speeds();
    LEVELS = levels();
    EXPLOSIONS = explosions();
    IMAGE_ASSETS = imageAssets();
    ASTEROIDS = asteroids();
    SPACESHIP_TYPE = spaceshipType();
}

export function setFPS(value) {
    value = parseInt(value);
    value = 1000 / value;
    if (value >= 16) {
        TIMERS.FRAME_TIME = value;
        reloadVariables();
    }
}

reloadVariables();
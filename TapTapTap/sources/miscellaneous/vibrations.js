import { Vibration } from 'react-native';

class GameVibration {
    constructor() {
        this.vibrationsOn = true;
    }

    shortVibration() {
        if (this.vibrationsOn) {
            Vibration.vibrate([0, 100]);
        }
    }

    mediumVibration() {
        if (this.vibrationsOn) {
            Vibration.vibrate([0, 200]);
        }
    }

    longVibration() {
        if (this.vibrationsOn) {
            Vibration.vibrate([0, 500]);
        }
    }

    cancelVibration() {
        Vibration.cancel();
    }

    setVibrationMode(mode) {
        this.vibrationsOn = mode;
    }

}

export var vibrations = new GameVibration();
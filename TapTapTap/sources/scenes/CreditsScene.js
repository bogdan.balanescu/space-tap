/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Text, View, TouchableWithoutFeedback, ScrollView } from 'react-native';
import { styles } from './../miscellaneous/styles.js'

import { sounds } from './../miscellaneous/sounds.js'

export class CreditsScene extends Component {
    constructor(props) {
        super(props);

        this.pressedButton = this.pressedButton.bind(this);
    }

    pressedButton(func) {
        sounds.buttonPress.replay();
        func();
    }

    render() {
        return (
            <ScrollView contentContainerStyle={styles.scrollableTransparentContainer}>
                <Text style={styles.title}>
                    Programmers
                </Text>
                <Text style={styles.text}>
                    Bogdan-Daniel Balanescu
                </Text>
                <Text style={styles.title}>
                    2D artists
                </Text>
                <Text style={styles.subtitle}>
                    App icon
                </Text>
                <Text style={styles.text}>
                    MillionthVector
                </Text>
                <Text style={styles.subtitle}>
                    Spaceships
                </Text>
                <Text style={styles.text}>
                    MillionthVector
                </Text>
                <Text style={styles.subtitle}>
                    Asteroids
                </Text>
                <Text style={styles.text}>
                    Eucalyp from www.flaticon.com
                </Text>
                <Text style={styles.text}>
                    Hand Drawn Goods from www.flaticon.com
                </Text>
                <Text style={styles.text}>
                    Freepik from www.flaticon.com
                </Text>
                <Text style={styles.subtitle}>
                    Bullets
                </Text>
                <Text style={styles.text}>
                    Freepik from www.flaticon.com
                </Text>
                <Text style={styles.subtitle}>
                    Buttons
                </Text>
                <Text style={styles.text}>
                    Dave Gandy from www.flaticon.com
                </Text>
                <Text style={styles.text}>
                    Egor Rumyantsev from www.flaticon.com
                </Text>
                <Text style={styles.title}>
                    Game Designers
                </Text>
                <Text style={styles.text}>
                    Bogdan-Daniel Balanescu
                </Text>
                <Text style={styles.title}>
                    Sound Designers
                </Text>
                <Text style={styles.subtitle}>
                    Music
                </Text>
                <Text style={styles.text}>
                    Eric Matyas
                </Text>
                <Text style={styles.text}>
                    www.soundimage.org
                </Text>
                <Text style={styles.subtitle}>
                    Sound FX
                </Text>
                <Text style={styles.text}>
                    www.zapsplat.com
                </Text>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.setMenuScene)}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            Back
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </ScrollView>
        );
    }
}


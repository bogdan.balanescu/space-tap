/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Text, View, TouchableWithoutFeedback } from 'react-native';
import { styles } from './../miscellaneous/styles.js'
import { EXPLOSIONS } from './../miscellaneous/variables.js';
import { ParticleExplosions } from './../environmentals/ParticleExplosions.js'

export class EndingScene extends Component {
    constructor(props) {
        super(props);

        this.onPress = this.onPress.bind(this);
    }

    componentDidMount() {
        var asteroids = this.props.asteroidsExplosionsPositions;
        for (var i = 0; i < asteroids.length; i++) {
            this.refs.explosions.addParticles(EXPLOSIONS.ASTEROID_EXPLOSION, {
                x: asteroids[i].x,
                y: asteroids[i].y
            });
        }
        var spaceship = this.props.spaceshipExplosionPosition;
        this.refs.explosions.addParticles(EXPLOSIONS.SPACESHIP_EXPLOSION, {
            x: spaceship.x + spaceship.width / 2,
            y: spaceship.y + spaceship.height / 2
        });
    }

    onPress() {
        this.props.replay();
    }

    render() {
        var highscoreText = this.props.score > this.props.highscore.score ?
            "This is a new highscore!" :
            this.props.highscore.username + " owns the highscore with " + this.props.highscore.score + " light years!";
        return (
            <TouchableWithoutFeedback onPress={this.onPress}>
                <View style={styles.transparentContainer}>
                    <ParticleExplosions ref="explosions" coordinatesOrientationBottom={true}>
                    </ParticleExplosions>
                    <Text style={styles.text}>
                        Congrats, {this.props.username}!
                    </Text>
                    <Text style={styles.text}>
                        You have made quite a journey.
                    </Text>
                    <Text style={styles.text}>
                        Your final score is: {this.props.score} light years ;)
                    </Text>
                    <Text style={styles.text}>
                        {highscoreText}
                    </Text>
                    <Text style={styles.text}>
                        Tap to play again :)
                    </Text>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}


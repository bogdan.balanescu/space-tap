/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, Text, Slider, TouchableWithoutFeedback } from 'react-native';
import { styles } from './../miscellaneous/styles.js'

import { sounds } from './../miscellaneous/sounds.js'

export class GraphicsScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            FPS: this.props.FPS,
            colorMode: this.props.colorMode
        };

        this.pressedButton = this.pressedButton.bind(this);
        this.setFPS = this.setFPS.bind(this);
        this.initialFPS = this.props.FPS;
        this.cancel = this.cancel.bind(this);

        this.setColorMode = this.setColorMode.bind(this);
        this.initialColorMode = this.props.colorMode;
    }

    pressedButton(func) {
        sounds.buttonPress.replay();
        this.setFPS();
        this.setColorMode();
        func();
    }

    setFPS() {
        this.props.setFPS(this.state.FPS);
    }

    setColorMode() {
        this.props.setColorMode(this.state.colorMode);
    }

    cancel() {
        this.props.setFPS(this.initialFPS);
        this.props.setColorMode(this.initialColorMode);
        this.props.setMenuScene();
    }

    render() {
        return (
            <View style={styles.transparentContainer}>
                <Text style={styles.subtitle}>
                    FPS
                </Text>
                {
                    this.state.FPS < 40 ?
                        <Text style={styles.text}>
                            Warning! Recommended 40 or more!
                        </Text> :
                        null
                }
                <Text style={styles.text}>
                    {parseInt(this.state.FPS)}
                </Text>
                <Slider style={styles.slider}
                        minimumValue={15}
                        maximumValue={60}
                        onValueChange={(value) => this.setState({FPS: value})}
                        value={this.props.FPS}>
                </Slider>
                <Text style={styles.subtitle}>
                    Day/Night Mode
                </Text>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(() => this.setState({colorMode: this.state.colorMode === 'day' ? 'night': 'day'}))}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            {this.state.colorMode === 'day' ? 'day': 'night'}
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.setMenuScene)}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            Save
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(this.cancel)}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            Back
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}


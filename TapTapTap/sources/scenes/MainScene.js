/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, AsyncStorage, BackAndroid, AppState } from 'react-native';
import { styles } from './../miscellaneous/styles.js'
import { TIMERS, SPACESHIP_TYPE } from './../miscellaneous/variables.js';
import { SpaceBackground } from './../environmentals/SpaceBackground.js'
import { ReadUsernameScene } from './ReadUsernameScene.js'
import { MenuScene } from './MenuScene.js'
import { TappingScene } from './TappingScene.js';
import { PlayingScene } from './PlayingScene.js';
import { EndingScene } from './EndingScene.js'
import { SettingsScene } from './SettingsScene.js'
import { CreditsScene } from './CreditsScene.js'
import { QuestionModal } from './../environmentals/QuestionModal.js'
import { SoundsScene } from './SoundsScene.js'
import { VibrationsScene } from './VibrationsScene.js'
import { GraphicsScene } from './GraphicsScene.js'

import { sounds } from './../miscellaneous/sounds.js'
import { vibrations } from './../miscellaneous/vibrations.js'
import { setFPS } from './../miscellaneous/variables.js'
import { setColorMode } from './../miscellaneous/styles.js'

export class MainScene extends Component {
    getDefaultState() {
        return {
            readUsername: false,
            menu: true,
            settings: false,
            sounds: false,
            vibrations: false,
            graphics: false,
            credits: false,
            count: 0,
            tappingAndPickingSpaceship: false,
            spaceshipType: undefined,
            playing: false,
            ending: false,
            modalVisible: false
        };
    }

    constructor() {
        super();
        this.state = this.getDefaultState();

        this.saveUsername = this.saveUsername.bind(this);
        this.setMenuScene = this.setMenuScene.bind(this);
        this.username = null;

        this.setPlayingScene = this.setPlayingScene.bind(this); // tappingAndPickingSpaceship
        this.setSettingsScene = this.setSettingsScene.bind(this);
        this.setCreditsScene = this.setCreditsScene.bind(this);
        this.setSoundsScene = this.setSoundsScene.bind(this);
        this.setVibrationsScene = this.setVibrationsScene.bind(this);
        this.setGraphicsScene = this.setGraphicsScene.bind(this);

        this.countPlusOne = this.countPlusOne.bind(this);
        this.onCountPress = this.countPlusOne;
        this.stopCount = this.stopCount.bind(this);
        this.pickSpaceship = this.pickSpaceship.bind(this);
        this.stopPickingSpaceship = this.stopPickingSpaceship.bind(this);

        this.gameOver = this.gameOver.bind(this);
        this.gameOverState = undefined;
        this.replay = this.replay.bind(this);

        this.changeUsername = this.changeUsername.bind(this);

        this.highscore = {
            username: "",
            score: 0
        };

        this.handleBackButton = this.handleBackButton.bind(this);
        this.openModal = this.openModal.bind(this);
        this.modalQuestion = "";
        this.modalClose = this.modalClose.bind(this);
        this.modalCancel = this.modalCancel.bind(this);

        this.handleAppStateChange = this.handleAppStateChange.bind(this);
        this.playSounds = this.playSounds.bind(this);
        this.stopSounds = this.stopSounds.bind(this);

        this.volume = '1';
        this.setVolumes = this.setVolumes.bind(this);

        this.vibrationsMode = 'true';
        this.setVibrationsMode = this.setVibrationsMode.bind(this);

        this.FPS = 60;
        this.setFPS = this.setFPS.bind(this);

        this.colorMode = 'night';
        this.setColorMode = this.setColorMode.bind(this);
    }

    componentWillMount() {
        AsyncStorage.getItem("username").then((value) => {
            this.username = value;
            if (value == null) {
                this.setState({
                    menu: false,
                    readUsername: true
                });
            }
        }).done();
        AsyncStorage.getItem("scores").then((scores) => {
            if (scores == null) {
                AsyncStorage.setItem("scores", JSON.stringify([]));
            }
            else {
                var scoresArray = JSON.parse(scores);
                for (var i = 0; i < scoresArray.length; i++) {
                    if (scoresArray[i].score > this.highscore.score) {
                        this.highscore = scoresArray[i];
                    }
                }
            }
        }).done();
        AsyncStorage.getItem("volume").then((value) => {
            if (value == null) {
                AsyncStorage.setItem("volume", '1');
            } else {
                this.volume = value;
                sounds.setVolume(this.volume);
            }
        }).done();
        AsyncStorage.getItem("vibrationsMode").then((value) => {
            if (value == null) {
                AsyncStorage.setItem("vibrationsMode", 'true');
            } else {
                this.vibrationsMode = value;
                vibrations.setVibrationMode(this.vibrationsMode === 'true');
            }
        }).done();
        AsyncStorage.getItem("FPS").then((value) => {
            if (value == null) {
                AsyncStorage.setItem("FPS", '16');
            } else {
                this.FPS = parseInt(value);
                setFPS(this.FPS);
            }
        }).done();
        AsyncStorage.getItem("colorMode").then((value) => {
            if (value == null) {
                AsyncStorage.setItem("colorMode", 'night');
            } else {
                this.colorMode = value;
                setColorMode(this.colorMode);
            }
        }).done();
    }

    setVolumes(value) {
        value = value.toString();
        sounds.setVolume(value);
        AsyncStorage.setItem("volume", value);
        this.volume = value;
    }

    setVibrationsMode(value) {
        vibrations.setVibrationMode(value === 'true');
        AsyncStorage.setItem("vibrationsMode", value);
        this.vibrationsMode = value;
    }

    setFPS(value) {
        setFPS(value);
        AsyncStorage.setItem("FPS", value.toString());
        this.FPS = value;
    }

    setColorMode(value) {
        setColorMode(value);
        AsyncStorage.setItem("colorMode", value);
        this.colorMode = value;
    }

    componentDidMount() {
        BackAndroid.addEventListener('hardwareBackPress', this.handleBackButton);
        AppState.addEventListener('change', this.handleAppStateChange);
    }

    componentWillUnmount() {
        BackAndroid.removeEventListener('hardwareBackPress', this.handleBackButton);
        AppState.removeEventListener('change', this.handleAppStateChange);
    }

    handleAppStateChange(appState) {
        if (appState == 'active') {
            this.playSounds();
        } else {
            this.stopSounds();
        }
    }

    handleBackButton() {
        if (this.state.menu) {
            this.openModal();
        }
        else if (this.state.readUsername || this.state.sounds || this.state.settings || this.state.credits || this.state.ending) {
            this.setMenuScene();
        }
        else if (this.state.tappingAndPickingSpaceship || this.state.playing) {
            // do nothing
        }
        return true;
    }

    openModal() {
        this.modalQuestion = "Are you sure you want to exit?";
        this.setState({
            modalVisible: true
        });
    }

    modalClose() {
        if (this.state.menu) {
            this.exitApp();
        }
        else {
            this.setMenuScene();
        }
    }

    modalCancel() {
        this.setState({
            modalVisible: false
        });
    }

    exitApp() {
        BackAndroid.exitApp(0);
    }

    saveUsername(username) {
        if (username == '') {
            return;
        }
        this.username = username;
        AsyncStorage.setItem("username", username);
        this.setMenuScene();
    }

    setMenuScene() {
        this.setState(this.getDefaultState());
    }

    setPlayingScene() {
        this.setState({
            menu: false,
            tappingAndPickingSpaceship: true
        });
    }

    setSettingsScene() {
        this.setState({
            menu: false,
            settings: true
        });
    }

    setCreditsScene() {
        this.setState({
            menu: false,
            credits: true
        });
    }

    countPlusOne() {
        var count =  this.state.count;
        if (count === 0) {
            setTimeout(this.stopCount, TIMERS.COUNTING_TAPS);
        }
        this.setState({count: this.state.count + 1});
    }

    stopCount() {
        this.onCountPress = function() {}; // undefined or empty function that does nothing
        this.pickSpaceship();
    }

    pickSpaceship() {
        // pick a monster
        if (this.state.count <= SPACESHIP_TYPE.SMALL.taps) {
            this.setState({spaceshipType: SPACESHIP_TYPE.SMALL});
        }
        else if (this.state.count <= SPACESHIP_TYPE.MEDIUM.taps) {
            this.setState({spaceshipType: SPACESHIP_TYPE.MEDIUM});
        }
        else if (this.state.count <= SPACESHIP_TYPE.LARGE.taps) {
            this.setState({spaceshipType: SPACESHIP_TYPE.LARGE});
        }
        else {
            this.setState({spaceshipType: SPACESHIP_TYPE.EPIC});
        }
        setTimeout(this.stopPickingSpaceship, TIMERS.PICKING_MONSTER);
    }

    stopPickingSpaceship() { // stop picking spaceship and set playing to true
        this.setState({
            tappingAndPickingSpaceship: false,
            playing: true
        });
    }

    gameOver(gameOverState) {
        this.gameOverState = gameOverState;
        AsyncStorage.getItem("scores").
        then((scores) => {
            return JSON.parse(scores);
        }).
        then((scores) => {
            scores.push({
                username: this.username,
                score: gameOverState.score
            });
            return scores;
        }).
        then((scores) => {
            return JSON.stringify(scores);
        }).
        then((scores) => {
            AsyncStorage.setItem("scores", scores);
        }).
        done();
        this.setState({
            playing: false,
            ending: true
        });
        if (this.gameOverState.score > this.highscore.score) {
            this.highscore = {
                username: this.username,
                score: this.gameOverState.score
            };
        }
    }

    replay() {
        this.setMenuScene();
        this.onCountPress = this.countPlusOne;
    }

    changeUsername() {
        this.setState({
            settings: false,
            readUsername: true
        });
    }

    setSoundsScene() {
        this.setState({
            settings: false,
            sounds: true
        })
    }

    setVibrationsScene() {
        this.setState({
            settings: false,
            vibrations: true
        });
    }

    setGraphicsScene() {
        this.setState({
            settings: false,
            graphics: true
        });
    }

    playSounds() {
        if (this.state.tappingAndPickingSpaceship == false &&
            this.state.playing == false &&
            this.state.ending == false) {
            sounds.playingSound.stop();
            sounds.mainMenu.play(-1);
        } else {
            sounds.mainMenu.stop();
            sounds.playingSound.play(-1);
        }
    }

    stopSounds() {
        sounds.mainMenu.stop();
        sounds.playingSound.stop();
    }

    render() {
        this.playSounds(); // TODO - play sounds
        return (
            <View style={styles.container}>
                <SpaceBackground>
                </SpaceBackground>
                <QuestionModal animated={true}
                       transparent={false}
                       visible={this.state.modalVisible}
                       question={this.modalQuestion}
                       close={this.modalClose}
                       cancel={this.modalCancel}>
                </QuestionModal>
                {
                    this.state.readUsername === true ?
                    <ReadUsernameScene saveUsername={this.saveUsername}
                                       cancelUsername={this.setMenuScene}
                                       username={this.username}
                                       hasCancel={this.username != null}>
                    </ReadUsernameScene> :
                    null
                }
                {
                    this.state.sounds === true ?
                    <SoundsScene setMenuScene={this.setMenuScene}
                                 setVolumes={this.setVolumes}
                                 volume={parseFloat(this.volume)}>
                    </SoundsScene> :
                    null
                }
                {
                    this.state.vibrations === true ?
                    <VibrationsScene setMenuScene={this.setMenuScene}
                                     setVibrationsScene={this.setVibrationsScene}
                                     setVibrationsMode={this.setVibrationsMode}
                                     vibrationsMode={this.vibrationsMode}>
                    </VibrationsScene> :
                    null
                }
                {
                    this.state.graphics === true ?
                    <GraphicsScene setMenuScene={this.setMenuScene}
                                   setFPS={this.setFPS}
                                   FPS={this.FPS}
                                   setColorMode={this.setColorMode}
                                   colorMode={this.colorMode}>
                    </GraphicsScene> :
                    null
                }
                {
                    this.state.menu === true ?
                    <MenuScene setPlayingScene={this.setPlayingScene}
                               setSettingsScene={this.setSettingsScene}
                               setCreditsScene={this.setCreditsScene}
                               exitApp={this.openModal}>
                    </MenuScene> :
                    null
                }
                {
                    this.state.tappingAndPickingSpaceship === true ?
                    <TappingScene onPress={this.onCountPress}
                                  count={this.state.count}>
                    </TappingScene> :
                    null
                }
                {
                    this.state.playing === true ?
                    <PlayingScene spaceshipType={this.state.spaceshipType}
                                  gameOver={this.gameOver}
                                  maxBullets={this.state.count}>
                    </PlayingScene> :
                    null
                }
                {
                    this.state.ending === true ?
                    <EndingScene asteroidsExplosionsPositions={this.gameOverState.asteroidsExplosionsPositions}
                                 spaceshipExplosionPosition={this.gameOverState.spaceshipExplosionPosition}
                                 score={this.gameOverState.score}
                                 highscore={this.highscore}
                                 username={this.username}
                                 replay={this.replay}>
                    </EndingScene> :
                    null
                }
                {
                    this.state.settings === true ?
                    <SettingsScene changeUsername={this.changeUsername}
                                   setGraphicsScene={this.setGraphicsScene}
                                   setVibrationsScene={this.setVibrationsScene}
                                   setSoundsScene={this.setSoundsScene}
                                   setMenuScene={this.setMenuScene}>
                    </SettingsScene> :
                    null
                }
                {
                    this.state.credits === true ?
                    <CreditsScene setMenuScene={this.setMenuScene}>
                    </CreditsScene> :
                    null
                }
            </View>
        );
    }
}


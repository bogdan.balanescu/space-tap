/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import { styles } from './../miscellaneous/styles.js'

import { sounds } from './../miscellaneous/sounds.js'

export class MenuScene extends Component {
    constructor(props) {
        super(props);
        this.pressedButton = this.pressedButton.bind(this);
    }

    pressedButton(func) {
        sounds.buttonPress.replay();
        func();
    }

    render() {
        return (
            <View style={styles.transparentContainer}>
                <View style={styles.transparentContainer}>
                    <Text style={styles.title}>
                        Space Tap
                    </Text>
                </View>
                <View style={styles.transparentContainer}>
                    <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.setPlayingScene)}>
                        <View style={styles.button}>
                            <Text style={styles.text}>
                                Play
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.setSettingsScene)}>
                        <View style={styles.button}>
                            <Text style={styles.text}>
                                Settings
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.setCreditsScene)}>
                        <View style={styles.button}>
                            <Text style={styles.text}>
                                Credits
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.exitApp)}>
                        <View style={styles.button}>
                            <Text style={styles.text}>
                                Exit
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
                <View style={styles.transparentContainer}>
                </View>
            </View>
        );
    }
}


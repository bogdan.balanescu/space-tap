/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableWithoutFeedback, Image } from 'react-native';
import { styles } from './../miscellaneous/styles.js'
import { TIMERS, SPACESHIP_TYPE, IMAGE_ASSETS, EXPLOSIONS, LEVELS } from './../miscellaneous/variables.js';
import { Spaceship } from './../actors/Spaceship.js';
import { Bullets } from './../actors/Bullets.js'
import { Asteroids } from './../actors/Asteroids.js'
import { ParticleExplosions } from './../environmentals/ParticleExplosions.js'
import { LevelInformation } from './../environmentals/LevelInformation.js'

import { sounds } from './../miscellaneous/sounds.js'
import { vibrations } from './../miscellaneous/vibrations.js'

import { Ammo } from './../actors/Ammo.js'

export class PlayingScene extends Component {
    constructor(props) {
        super(props);

        this.onPressInRight = this.onPressInRight.bind(this);
        this.onPressOutRight = this.onPressOutRight.bind(this);
        this.right = false;

        this.onPressInLeft = this.onPressInLeft.bind(this);
        this.onPressOutLeft = this.onPressOutLeft.bind(this);
        this.left = false;

        this.fireBullet = this.fireBullet.bind(this);
        this.maxY = Dimensions.get('window').height;

        this.getLevel = this.getLevel.bind(this);
        this.setGameOver = this.setGameOver.bind(this);

        this.renderInterval = undefined;
        this.updateScene = this.updateScene.bind(this);
        this.moveActors = this.moveActors.bind(this);
        this.checkCollisions = this.checkCollisions.bind(this);
        this.moveSpaceship = this.moveSpaceship.bind(this);
        this.moveBullets = this.moveBullets.bind(this);
        this.moveAsteroids = this.moveAsteroids.bind(this);
        this.moveAmmo = this.moveAmmo.bind(this);
        this.checkBulletsHitAsteroids = this.checkBulletsHitAsteroids.bind(this);
        this.checkAsteroidsHitSpaceship = this.checkAsteroidsHitSpaceship.bind(this);
        this.checkAmmoHitSpaceship = this.checkAmmoHitSpaceship.bind(this);
    }

    componentDidMount() {
        this.renderInterval = setInterval(this.updateScene, TIMERS.FRAME_TIME);
    }

    componentWillUnmount() {
        clearInterval(this.renderInterval);
    }

    onPressInRight() {
        this.right = true;
    }

    onPressOutRight() {
        this.right = false;
    }

    onPressInLeft() {
        this.left = true;
    }

    onPressOutLeft() {
        this.left = false;
    }

    fireBullet(event) {
        var lastPosition = this.refs.spaceship.getPosition();
        lastPosition.x += this.props.spaceshipType.style.width / 2 - 4; // -4 because of bullet width is 8 // TODO - refactor - don't hardcode
        lastPosition.y += this.props.spaceshipType.style.height;
        this.refs.explosions.addParticles(EXPLOSIONS.FIRING_EXPLOSION, lastPosition);
        lastPosition.y += -16; // for bullet to come from within the spaceship // TODO - refactor - don't hardcode
        var fired = this.refs.bullets.addBullet(1, lastPosition);
        if (fired) {
            sounds.fireBullet.replay();
            this.refs.levelInformation.addBulletCount(1);
        }
    }

    updateScene() {
        this.moveActors();
        this.checkCollisions();
    }

    moveActors() {
        this.moveSpaceship();
        this.moveBullets();
        this.moveAsteroids();
        this.moveAmmo();
    }

    moveSpaceship() {
        if (this.right) {
            this.refs.spaceship.move(this.props.spaceshipType.movingDistance);
        }
        if (this.left) {
            this.refs.spaceship.move(-this.props.spaceshipType.movingDistance);
        }
    }

    moveBullets() {
        this.refs.bullets.updateBullets();
    }

    moveAsteroids() {
        this.refs.asteroids.updateAsteroids();
    }

    moveAmmo() {
        this.refs.ammo.updateAmmo();
    }

    checkCollisions() {
        this.checkBulletsHitAsteroids();
        this.checkAsteroidsHitSpaceship();
        this.checkAmmoHitSpaceship();
    }

    checkCollision(rect1, rect2){
        if (rect1.x < rect2.x + rect2.width &&
            rect1.x + rect1.width > rect2.x &&
            rect1.y < rect2.y + rect2.height &&
            rect1.height + rect1.y > rect2.y) {
            return true;
        }
        return false;
    }

    checkBulletsHitAsteroids() {
        var bullets = this.refs.bullets.getBulletsPositions();
        var asteroids = this.refs.asteroids.getAsteroidsPositions();
        for (var i=0; i < bullets.length; i++) {
            for (var j=0; j < asteroids.length; j++) {
                if (this.checkCollision(bullets[i], asteroids[j])) {
                    sounds.asteroidExplosion.replay();
                    vibrations.mediumVibration();
                    this.refs.explosions.addParticles(EXPLOSIONS.ASTEROID_EXPLOSION, {
                        x: (bullets[i].x + asteroids[j].x) / 2,
                        y: (bullets[i].y + asteroids[j].y) / 2
                    });
                    this.refs.bullets.destroyBulletAtIndex(i);
                    this.refs.ammo.addAmmo(asteroids[j]);
                    this.refs.asteroids.destroyAsteroidAtIndex(j);
                    this.refs.levelInformation.addScore(50); // TODO - refactor - don't hardcode
                }
            }
        }
    }

    checkAsteroidsHitSpaceship() {
        var asteroids = this.refs.asteroids.getAsteroidsPositions();
        var spaceship = this.refs.spaceship.getCollisionPosition();
        for (var i=0; i < asteroids.length; i++) {
            if (this.checkCollision(asteroids[i], spaceship)) {
                sounds.asteroidExplosion.replay();
                vibrations.longVibration();
                this.setGameOver(); // makes everything explode
                return;
            }
        }
    }

    checkAmmoHitSpaceship() {
        var ammo = this.refs.ammo.getAmmoPositions();
        var spaceship = this.refs.spaceship.getCollisionPosition();
        for (var i=0; i < ammo.length; i++) {
            if (this.checkCollision(ammo[i], spaceship)) {
                this.refs.ammo.destroyAmmoAtIndex(i);
                sounds.ammoCollected.replay();
                vibrations.shortVibration();
                this.refs.levelInformation.addToMaxBullets(1);
                this.refs.bullets.addToMaxBullets(1);
            }
        }
    }

    getLevel() {
        return this.refs.levelInformation.getLevel();
    }

    setGameOver() {
        clearInterval(this.renderInterval);
        this.refs.explosions.stopRendering();
        this.refs.levelInformation.stopRendering();
        var asteroids = this.refs.asteroids.getAsteroidsPositions();
        var spaceship = this.refs.spaceship.getCollisionPosition();
        var score = this.refs.levelInformation.getScore();
        setTimeout(function() {
            this.props.gameOver({
                asteroidsExplosionsPositions: asteroids,
                spaceshipExplosionPosition: spaceship,
                score: score
            });
        }.bind(this), TIMERS.ENDING_GAME);
    }

    render() {
        return (
            <View style={styles.transparentContainer}>
                <ParticleExplosions ref="explosions" coordinatesOrientationBottom={true}>
                </ParticleExplosions>
                <LevelInformation ref="levelInformation" initialBulletsCount={this.props.maxBullets}>
                </LevelInformation>
                <Spaceship ref="spaceship" spaceshipType={this.props.spaceshipType}>
                </Spaceship>
                <Bullets ref="bullets" maxBullets={this.props.maxBullets}>
                </Bullets>
                <Asteroids ref="asteroids" getLevel={this.getLevel}>
                </Asteroids>
                <Ammo ref="ammo">
                </Ammo>
                <TouchableWithoutFeedback onPress={this.fireBullet}>
                    <Image source={IMAGE_ASSETS.FIRE_BUTTON}
                           style={{position: 'absolute', bottom: 64, left: 0, opacity: 0.5}}>
                    </Image>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPressIn={this.onPressInLeft} onPressOut={this.onPressOutLeft}>
                    <Image source={IMAGE_ASSETS.LEFT_ARROW}
                           style={{position: 'absolute', bottom: 0, left: 0, opacity: 0.5}}>
                    </Image>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={this.fireBullet}>
                    <Image source={IMAGE_ASSETS.FIRE_BUTTON}
                           style={{position: 'absolute', bottom: 64, right: 0, opacity: 0.5}}>
                    </Image>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPressIn={this.onPressInRight} onPressOut={this.onPressOutRight}>
                    <Image source={IMAGE_ASSETS.RIGHT_ARROW}
                           style={{position: 'absolute', bottom: 0, right: 0, opacity: 0.5}}>
                    </Image>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}


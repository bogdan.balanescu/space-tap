/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, Text, TextInput, TouchableWithoutFeedback } from 'react-native';
import { styles } from './../miscellaneous/styles.js'

import { sounds } from './../miscellaneous/sounds.js'

export class ReadUsernameScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ''
        };
        this.saveUsername = this.saveUsername.bind(this);
        this.pressedButton = this.pressedButton.bind(this);
    }

    saveUsername() {
        this.props.saveUsername(this.state.text);
    }

    pressedButton(func) {
        sounds.buttonPress.replay();
        func();
    }

    render() {
        return (
            <View style={styles.transparentContainer}>
                {
                    this.props.hasCancel == true ?
                        <Text style={styles.text}>
                            Hi, {this.props.username}! :)
                        </Text> :
                        <Text style={styles.text}>
                            Hi. Welcome to Space Tap! :)
                        </Text>
                }
                <Text style={styles.text}>
                    What should we call you?
                </Text>
                <TextInput style={styles.textInput}
                           placeholder="Type your username here"
                           autoFocus={true}
                           onChangeText={(text) => this.setState({text})}>
                </TextInput>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(this.saveUsername)}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            Save
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                {
                    this.props.hasCancel == true ?
                    <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.cancelUsername)}>
                        <View style={styles.button}>
                            <Text style={styles.text}>
                                Cancel
                            </Text>
                        </View>
                    </TouchableWithoutFeedback> :
                    null
                }
            </View>
        );
    }
}


/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Text, View, TouchableWithoutFeedback } from 'react-native';
import { styles } from './../miscellaneous/styles.js'

import { sounds } from './../miscellaneous/sounds.js'

export class SettingsScene extends Component {
    constructor(props) {
        super(props);

        this.pressedButton = this.pressedButton.bind(this);
    }

    pressedButton(func) {
        sounds.buttonPress.replay();
        func();
    }

    render() {
        return (
            <View style={styles.transparentContainer}>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.setGraphicsScene)}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            Graphics
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.setVibrationsScene)}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            Vibrations
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.setSoundsScene)}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            Sounds
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.changeUsername)}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            Change Username
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.setMenuScene)}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            Back
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}


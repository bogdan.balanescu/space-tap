/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, Text, Slider, TouchableWithoutFeedback } from 'react-native';
import { styles } from './../miscellaneous/styles.js'

import { sounds } from './../miscellaneous/sounds.js'

export class SoundsScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.volume
        };

        this.pressedButton = this.pressedButton.bind(this);
        this.setVolumes = this.setVolumes.bind(this);
        this.initialVolume = this.props.volume;
        this.cancel = this.cancel.bind(this);
    }

    pressedButton(func) {
        sounds.buttonPress.replay();
        func();
    }

    setVolumes() {
        this.props.setVolumes(this.state.value);
    }

    cancel() {
        this.props.setVolumes(this.initialVolume);
        this.props.setMenuScene();
    }

    render() {
        this.setVolumes();
        return (
            <View style={styles.transparentContainer}>
                <Text style={styles.subtitle}>
                    Sound Volume
                </Text>
                <Slider style={styles.slider}
                        minimumValue={0}
                        maximumValue={1}
                        onValueChange={(value) => this.setState({value: value})}
                        value={this.props.volume}
                        onSlidingComplete={this.setVolumes}>
                </Slider>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.setMenuScene)}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            Save
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(this.cancel)}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            Back
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}


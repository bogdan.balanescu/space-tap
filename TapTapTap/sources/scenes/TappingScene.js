/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import { styles } from './../miscellaneous/styles.js'
import { TIMERS, EXPLOSIONS } from './../miscellaneous/variables.js'
import { ParticleExplosions } from './../environmentals/ParticleExplosions.js'

import { sounds } from './../miscellaneous/sounds.js'

export class TappingScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            particles: [],
            tapping: true,
            pickingMonster: false
        };
        this.onPress = this.onPress.bind(this);

        this.stopTapping = this.stopTapping.bind(this);
    }

    stopTapping() {
        this.onPress = function() {}; // undefined or empty function that does nothing
        this.setState({tapping: false, pickingMonsters: true});
    }

    onPress(event) {
        if (this.props.count == 0) {
            setTimeout(this.stopTapping, TIMERS.COUNTING_TAPS);
        }
        this.props.onPress();
        var lastPosition = {x: event.nativeEvent.pageX, y: event.nativeEvent.pageY};
        this.refs.explosions.addParticles(EXPLOSIONS.TAPPING_EXPLOSION, lastPosition);
    }

    render() {
        var message = this.props.count === 0 ? 'Tap as fast as you can to pick a spaceship' : this.props.count + ' taps so far';
        message = this.state.tapping === false || this.state.pickingMonster === true ?
                'Picking your spaceship...':
                message ;
        return (
            <TouchableWithoutFeedback onPress={(event) => {sounds.asteroidExplosion.replay(); this.onPress(event);}}>
                <View style={styles.transparentContainer}>
                    <Text style={styles.text}>
                        {message}
                    </Text>
                    <ParticleExplosions ref="explosions" coordinatesOrientationBottom={false}>
                    </ParticleExplosions>
                </View>
            </TouchableWithoutFeedback>
        );
    }
}


/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { View, Text, SwitchAndroid, TouchableWithoutFeedback, AsyncStorage } from 'react-native';
import { styles } from './../miscellaneous/styles.js'

import { sounds } from './../miscellaneous/sounds.js'
import { vibrations } from './../miscellaneous/vibrations.js'

export class VibrationsScene extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.vibrationsMode
        };

        this.pressedButton = this.pressedButton.bind(this);
        this.setVibrationsMode = this.setVibrationsMode.bind(this);
        this.initialMode = this.props.vibrationsMode;
        this.cancel = this.cancel.bind(this);
    }

    pressedButton(func) {
        sounds.buttonPress.replay();
        func();
    }

    setVibrationsMode() {
        this.props.setVibrationsMode(this.state.value);
    }

    cancel() {
        this.props.setVibrationsMode(this.initialMode);
        this.props.setMenuScene();
    }

    render() {
        this.setVibrationsMode();
        return (
            <View style={styles.transparentContainer}>
                <Text style={styles.subtitle}>
                    Vibrations Mode
                </Text>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(() => this.setState({value: this.state.value === 'true' ? 'false': 'true'}))}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            {this.state.value === 'true' ? 'On': 'Off'}
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(this.props.setMenuScene)}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            Save
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => this.pressedButton(this.cancel)}>
                    <View style={styles.button}>
                        <Text style={styles.text}>
                            Back
                        </Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

